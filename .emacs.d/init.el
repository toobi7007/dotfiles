(require 'package) 
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
			 ("org" . "https://orgmode.org/elpa/")
			 ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

(use-package all-the-icons)
;; Zum Installieren der fonts: all-the-cons-install-fonts

(use-package evil
  :init
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  (setq evil-want-C-u t)
  (setq evil-want-C-i-jump nil)
  :config
  (evil-mode t)
  ;; Stadien wo ich gerne den normalen Emacs Mode haben wuerde
  (dolist (state '(speedbar-mode compilation-mode
		   dired-mode xref--xref-buffer-mode
		   shell-mode eshell
		   vterm-mode term-mode
		   shell docker-container-mode
		   docker-image-mode docker-network-mode
		   docker-volume-mode Buffer-menu-mode
		   ebib-index-mode ebib-strings-mode
		   ebib-entry-mode ebib-log-mode
		   ebib-multiline-mode))
    (evil-set-initial-state state 'emacs))
  ;; Globale Shortcuts
  (evil-define-key 'normal prog-mode-map ",C" 'projectile-compile-project)
  (evil-define-key 'normal prog-mode-map ",c" 'compile)
  (evil-define-key 'normal prog-mode-map ",D" 'projectile-run-gdb)
  (evil-define-key 'normal prog-mode-map ",d" 'gdb)
  (evil-define-key 'normal prog-mode-map ",f" 'projectile-find-file-dwim)
  (evil-define-key 'normal prog-mode-map ",g" 'magit-status)
  (evil-define-key 'visual 'global ",r" 'replace-string))

(setq org-babel-python-command "python3")
(add-hook 'python-mode-hook
  (lambda () (setq indent-tabs-mode t)))


(use-package popper
  :init
  (evil-define-key 'normal 'global (kbd "M-,") 'popper-toggle-latest)
  (evil-define-key 'insert 'global (kbd "M-,") 'popper-toggle-latest)

  (evil-define-key 'normal 'global (kbd "C-,") 'popper-cycle)
  (evil-define-key 'insert 'global (kbd "C-,") 'popper-cycle)

  (evil-define-key 'normal 'global (kbd "C-M-,") 'popper-toggle-type)
  (evil-define-key 'insert 'global (kbd "C-M-,") 'popper-toggle-type)

  (popper-mode +1)
  (popper-echo-mode +1))

(use-package evil-collection
  :config
  (evil-collection-init '(vterm dired ebib ibuffer help)))

(use-package vterm)

;; Tide fuer Typescript
(if (executable-find "node")
    (progn
      (use-package tide
	:config
	(evil-define-key 'normal 'global (kbd "M-.") 'tide-jump-to-definition)
	(evil-define-key 'normal 'global (kbd "C-SPC") 'company-complete-common))
      (add-hook 'typescript-mode-hook (lambda ()
					(interactive)
					 (tide-setup)
					 (flycheck-mode +1)
					 (tide-hl-identifier-mode +1)
					 (company-mode +1)))
      (add-hook 'rjsx-mode-hook (lambda ()
				  (interactive)
				  (tide-setup)
				  (flycheck-mode +1)
				  (tide-hl-identifier-mode +1)
				  (company-mode +1)))
      (add-hook 'before-save-hook 'tide-format-before-save)))

(use-package bm
  :config
  (evil-define-key 'normal 'global " mm" 'bm-toggle)
  (evil-define-key 'normal 'global " mj" 'bm-next)
  (evil-define-key 'normal 'global " mk" 'bm-previous))

;; (use-package flycheck)

(use-package embark
  :bind
  (("C-c o" . embark-act)
   ("C-c e" . embark-dwim)))

(use-package counsel
  :bind (("M-x" . counsel-M-x)
	 ("C-x b" . counsel-switch-buffer)
	 ("C-x C-f" . counsel-find-file)))

(use-package perspective
 :init (persp-mode)
 :bind ("C-x b" . persp-ivy-switch-buffer)
 :custom
 (persp-mode-prefix-key (kbd "C-c M-p")) 
 :config
 (evil-define-key 'normal 'global " b" 'ibuffer)
 (evil-define-key 'normal 'global " B" 'persp-ibuffer))

;; (setq org-clock-persist 'history)
;; (org-clock-persistence-insinuate)
(add-hook 'org-mode-hook 'display-fill-column-indicator-mode)
(add-hook 'org-mode-hook 'auto-fill-mode)
(setq org-latex-caption-above nil)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (python . t)
   (C . t)))


(setq org-todo-keywords
      '((sequence "TODO" "IN ARBEIT" "|" "DONE")))

;; (use-package org-ref
;;   :config
;;   (define-key org-mode-map (kbd "C-c ]") 'org-ref-insert-link)
;;   (evil-define-key 'normal 'bibtex-mode-map (kbd "M-b") 'org-ref-bibtex-hydra/body))

;; (with-eval-after-load "ox-latex"
;;   (add-to-list 'org-latex-classes
;;                '("scrreprt" "\\documentclass{scrreprt}"
;;                  ("\\section{%s}" . "\\section*{%s}")
;;                  ("\\subsection{%s}" . "\\subsection*{%s}")
;;                  ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
;;                  ("\\paragraph{%s}" . "\\paragraph*{%s}")
;;                  ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))))

(use-package org-bullets
  :config (add-hook 'org-mode-hook (lambda()
				     (org-bullets-mode 1))))

;; (use-package ivy-bibtex
;;   :init
;;   (setq bibtex-completion-bibliography '("~/Desktop/wissArbeit/notes/all.bib")
;; 	bibtex-completion-library-path '("~/wiss/pdfs")
;; 	bibtex-completion-notes-path "~/Desktop/wissArbeit/notes"))

;; (setq org-latex-pdf-process
;;       '("pdflatex -interaction nonstopmode -output-directory %o %f"
;; 	"bibtex %b"
;; 	"pdflatex -interaction nonstopmode -output-directory %o %f"
;; 	"pdflatex -interaction nonstopmode -output-directory %o %f"))

;; (use-package ebib
;;   :config
;;   (evil-define-key 'normal 'global " l" 'ebib)
;;   (setq ebib-notes-directory "~/Desktop/wissArbeit/notes/")
;;   (setq ebib-file-associations '(("pdf" . "open") ("ps" . "gv"))))

(use-package org-roam
  :init
  (setq org-roam-v2-ack t)
  :custom
  (org-roam-directory "~/org-roam")
  (org-roam-completion-everywhere t)
  :bind (("C-c n l" . org-roam-buffer-toggle)
	 ("C-c n f" . org-roam-node-find)
	 ("C-c n i" . org-roam-node-insert)
	 ("C-c n j" . org-roam-dailies-capture-today)
	 ("C-c n c" . org-roam-capture)
	 :map org-mode-map
	 ("C-M-i" . completion-at-point)) 
  :config
  (org-roam-setup))

(use-package docker
  :bind ("C-c d" . docker))

;; (use-package lsp-ui
;;   :hook (lsp-mode . lsp-ui-mode)
;;   :custom
;;   (lsp-ui-doc-position 'bottom))

;; (use-package company
;;   :bind (:map company-active-map
;; 	      ("RET" . company-complete-selection)
;; 	      ("C-SPC" . keyboard-quite))
;;   :config 
;;   (evil-define-key 'insert 'global (kbd "C-SPC") 'company-indent-or-complete-common)
;;   (setq company-idle-delay 1)
;;   (setq company-show-numbers t))

;; (use-package company-box
;;   :hook (company-mode . company-box-mode))

(use-package which-key
  :init (which-key-mode)
  :diminish which-key-mode)

(use-package nyan-mode
  :init 
  (nyan-mode)
  :config 
  (nyan-start-animation)
  (nyan-toggle-wavy-trail))

(use-package dired
  :ensure nil
  :commands (dired dired-jump)
  :hook (dired-mode-hook . evil-mode)
  :bind (:map dired-mode-map
	      ("j" . dired-next-line)
	      ("k" . dired-previous-line)
	      ("h" . dired-up-directory)
	      ("l" . dired-find-file)
	      ("J" . dired-goto-file)
	      (")" . dired-hide-dotfiles-mode))
  :custom ((dired-listing-switches "-agho")))

(use-package dired-hide-dotfiles
  :hook (dired-mode . dired-hide-dotfiles-mode))

(use-package all-the-icons-dired
  :hook (dired-mode . all-the-icons-dired-mode))

(use-package highlight-indent-guides
  :init
  (add-hook 'prog-mode-hook 'highlight-indent-guides-mode)
  (setq highlight-indent-guides-method 'character))

(use-package magit)

(use-package elpy)

(use-package cider)

(use-package python-mode
  :custom
  (python-shell-interpreter "python3"))

(dolist (mode '(term-mode-hook
		shell-mode-hook
		vterm-mode-hook
		eshell-mode-hook
		org-mode-hook))
  (add-hook mode (lambda ()
		   (display-line-numbers-mode 0)
		   (hl-line-mode 0))))

(use-package smartparens
  :hook (prog-mode . smartparens-mode)
  :config
  (evil-define-key 'visual prog-mode-map ",("
    'sp-wrap-round)
  (evil-define-key 'visual prog-mode-map ",{"
    'sp-wrap-curly)
  (evil-define-key 'visual prog-mode-map ",["
    'sp-wrap-square)
  (evil-define-key 'insert prog-mode-map
    (kbd "C-c C-a") 'sp-beginning-of-sexp)
  (evil-define-key 'insert prog-mode-map
    (kbd "C-c C-e") 'sp-end-of-sexp)
  (evil-define-key 'insert prog-mode-map
    (kbd "C-c a") 'sp-backward-up-sexp)
  (evil-define-key 'insert prog-mode-map
    (kbd "C-c e") 'sp-up-sexp)
  (evil-define-key 'normal prog-mode-map
    (kbd "C-c C-r") 'sp-rewrap-sexp)
  (evil-define-key 'normal prog-mode-map
    (kbd "C-c C-k") 'sp-splice-sexp)
  (evil-define-key 'normal prog-mode-map
    (kbd "C-c C-l") 'sp-forward-slurp-sexp)
  (evil-define-key 'normal prog-mode-map
    (kbd "C-c C-h") 'sp-forward-blarf-sexp)
  (evil-define-key 'normal prog-mode-map
    (kbd "C-c C-c") 'comment-or-uncomment-region)

  (evil-define-key 'normal prog-mode-map " me" 'sp-end-of-sexp)
  (evil-define-key 'normal prog-mode-map " mE" 'sp-up-sexp)
  (evil-define-key 'normal prog-mode-map " ma" 'sp-beginning-of-sexp)
  (evil-define-key 'normal prog-mode-map " mA" 'sp-backward-up-sexp)
  (evil-define-key 'normal prog-mode-map " mn" 'sp-down-sexp)
  (evil-define-key 'normal prog-mode-map " ml" 'sp-forward-sexp)
  (evil-define-key 'normal prog-mode-map " mh" 'sp-backward-sexp)
  (evil-define-key 'normal prog-mode-map " rw" 'sp-rewrap-sexp)
  (evil-define-key 'normal prog-mode-map " rk" 'sp-splice-sexp)
  (evil-define-key 'normal prog-mode-map " sl" 'sp-forward-slurp-sexp)
  (evil-define-key 'normal prog-mode-map " sh" 'sp-forward-barf-sexp)
  (evil-define-key 'normal prog-mode-map " sH" 'sp-backward-slurp-sexp)
  (evil-define-key 'normal prog-mode-map " sL" 'sp-backward-blarf-sexp)
  (evil-define-key 'normal prog-mode-map ",," 'cider-eval-defun-at-point))

(global-display-line-numbers-mode 1)
(menu-bar-mode -1)
(scroll-bar-mode -1)
(tool-bar-mode -1)
(add-hook 'prog-mode-hook 'hl-line-mode)
(fset 'yes-or-no-p 'y-or-n-p)

;; Funktion um die Shell Umegbungsvariablen auf Emacs zu uebertragen
(defun set-exec-path-from-shell-PATH ()
  (interactive)
  (let ((path-from-shell (replace-regexp-in-string
			  "[ \t\n]*$" "" (shell-command-to-string
					  "$SHELL --login -c 'echo $PATH'"))))
    (setenv "PATH" path-from-shell)
    (setq exec-path (split-string path-from-shell path-separator))))


(use-package doom-themes
  :config (load-theme 'doom-snazzy t))
;; doom-ephemeral, doom-ir-black, doom-outrun-electric, doom-snazzy, doom-Iosvkem
;; (use-package tron-legacy-theme
;; 	:config (load-theme 'tron-legacy t))

(use-package ivy-rich
  :init (ivy-rich-mode 1)
  :after counsel
  :config
  (setq ivy-format-function #'ivy-format-function-line))

;;/Users/alex/.nix-profile/bin:/nix/var/nix/profiles/default/bin:

;; Falls Macbook, dann anderes Theme + mac-spezifische Sachen
(if (string= system-type "darwin")
    (progn
      (set-face-attribute 'default nil :height 150)
      (setq mac-option-key-is-meta nil
	    mac-option-modifier 'control 
	    mac-command-key-is-meta t
	    mac-command-modifier 'meta)
      (setenv "PATH" "/Users/alex/.nix-profile/bin:/nix/var/nix/profiles/default/bin:/opt/homebrew/opt/openssl@3/bin:/opt/homebrew/opt/libpq/bin:/Users/alex/.nvm/versions/node/v16.15.0/bin:/opt/homebrew/opt/openjdk/bin:/opt/homebrew/bin:/opt/homebrew/sbin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/Library/TeX/texbin:/opt/X11/bin:/Library/Apple/usr/bin:/Users/alex/.cargo/bin")))


;; Globale Shortcuts
(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-x k") 'kill-this-buffer)

;; Orgmode Timetracking
(global-set-key (kbd "C-c C-x C-j") 'org-clock-goto)
(global-set-key (kbd "C-c C-x C-o") 'org-clock-out)
(global-set-key (kbd "C-c C-x C-x") 'org-clock-in-last)

;; Generelle Sachen
(setq inhibit-splash-screen t
      cursor-type 'bar
      visible-bell t
      mouse-wheel-scroll-amount '(3 ((shift) . 3)))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package ivy
  :diminish
  :bind (:map ivy-minibuffer-map
	      ("TAB" . ivy-alt-done)
	      ("C-j" . ivy-next-line)
	      ("C-l" . ivy-alt-done)
	      ("C-k" . ivy-previous-line)
	 :map ivy-switch-buffer-map
	      ("C-k" . ivy-previous-line)
	      ("C-d" . ivy-switch-buffer-kill))
  :init
  (ivy-mode))

(use-package swiper
  :init
  (global-set-key (kbd "C-s") 'swiper))

(evil-define-key 'normal 'global (kbd "C-j") 'electric-newline-and-maybe-indent)

(use-package drag-stuff
  :hook (text-mode . drag-stuff-mode)
  :config
  (evil-define-key 'normal 'global (kbd "C-k") 'drag-stuff-up))

;; (use-package lsp-mode
;;   :commands (lsp lsp-deferred)
;;   :hook
;;   (lsp-mode . lsp-mode-setup)
;;   (lsp-mode . global-flycheck-mode)
;;   (lsp-mode . yas-minor-mode-on)
;;   (java-mode . lsp-deferred)
;;   (c++-mode . lsp-deferred)
;;   (c-mode . lsp-deferred)
;;   :init
;;   (setq lsp-keymap-prefix "C-c l")

;;   :config
;;   (lsp-enable-which-key-integration t)
;;   (setq lsp-ui-sideline-enable nil
;;         lsp-ui-sideline-show-code-actions nil
;;         lsp-signature-render-documentation nil)
;;   (setq lsp-completion-provider :none)
;;   (lsp-register-client
;;    (make-lsp-client :new-connection (lsp-tramp-connection "clangd")
;; 		    :major-modes '(c-mode c++-mode)
;; 		    :remote? t
;; 		    :server-id 'clangd-remote)))

;; Cpp Stuff
(use-package yasnippet
  :init (yas-global-mode 1)
  :config
  (setq yas-snippet-dirs '("~/.dotfiles/.emacs.d/snippets/"))
  (evil-define-key 'insert 'global (kbd "C-SPC") 'yas-expand)
  (global-set-key (kbd "C-c C-y") 'yas-insert-snippet))

(use-package yasnippet-snippets)

(use-package ace-jump-mode
  :bind ("C-M-," . ace-jump-mode))

(use-package doom-modeline
  :init
  (doom-modeline-mode 1)
  :custom ((doom-modeline-height 10)))

(use-package projectile
  :init
  (projectile-mode +1)
  :bind-keymap
  ("C-c p" . projectile-command-map))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(smooth-scrolling slime yasnippet-snippets which-key use-package tide smartparens rust-mode rjsx-mode request rainbow-delimiters python-mode projectile popper perspective org-roam org-ref org-bullets nyan-mode nix-mode multi-vterm magit lua-mode lsp-ui ivy-rich ivy-bibtex highlight-indent-guides helm evil-collection embark elpy ebib drag-stuff doom-themes doom-modeline docker dired-hide-dotfiles counsel company-box cmake-mode cider bm all-the-icons-dired ace-jump-mode)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
