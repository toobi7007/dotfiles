# Installieren der Standardpackages
gruen='\033[0;32m'
rot='\033[0;31m'
nc='\033[0m'
fehleranzeige() {
    echo "\n\nSiehe FEHLER im aktuellen Verzeichnis."
    exit
}

echo "Installiere notwendige Programme:"
sudo apt-get install rofi 1> /dev/null 2> FEHLER
if [ $? -eq 0 ]; then
    echo "\t${gruen}rofi erfolgreich installiert.${nc}"
    echo "\t  vers." $(rofi -v)
else
    echo -n "\t${rot}rofi konnte nicht installiert werden.${nc}"
    fehleranzeige
fi

# Setzen von Konfigurationen
simple_fehler_check() {
    if [ $? -eq 0 ]; then
	echo "\t${gruen}CHECK${nc}"
    else
	echo -n "\t${rot}FEHLER${nc}"
	fehleranzeige
    fi
}

echo "\n"

echo -n "Klonen der dotfiles..."
git clone --quiet https://gitlab.com/toobi7007/dotfiles /tmp/.dotfiles 2> FEHLER
simple_fehler_check

echo "Setzen der neuen Standardshell... "
chsh -s $(which zsh)
echo -n " --->" && simple_fehler_check
